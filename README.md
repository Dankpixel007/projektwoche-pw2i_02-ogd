# README.md

## Ausbildung der Wohnbevölkerung in der Schweiz

### Autoren:
- Glenn Meijerink
- Saheser Seref

### Betreuende Lehrpersonen:
- Martin Hager
- Silvan Hofstetter

### Informatikmittelschule Frauenfeld

## Projektbeschreibung
Dieses Projekt wurde im Rahmen der Projektwoche an der Informatikmittelschule Frauenfeld durchgeführt. Ziel des Projekts war die Entwicklung einer Webanwendung zur Visualisierung von Daten zum Ausbildungsstand der Schweizer Wohnbevölkerung.

## Inhaltsverzeichnis
1. [Einleitung](#einleitung)
2. [Technologien](#technologien)
3. [Installation](#installation)
4. [Benutzung](#benutzung)
5. [Funktionen](#funktionen)
6. [Architektur](#architektur)
7. [Testfälle](#testfälle)
8. [Quellen](#quellen)
9. [Screenshots](#screenshots)

## Einleitung
Dieses Dokument dient als Übersicht und Anleitung für das Projekt "Ausbildung der Wohnbevölkerung in der Schweiz". Die Applikation stellt Daten in drei verschiedenen Diagrammen dar: einem Balkendiagramm und zwei Kuchendiagrammen. Zusätzlich wird eine interaktive Karte der Schweiz mit Tooltip-Anzeigen integriert.

## Technologien
- **Backend:** Node.js, Express
- **Frontend:** HTML, CSS, JavaScript, Chart.js
- **Daten:** JSON-Daten von OpenGovernmentData (opendata.swiss)
- **Weitere Tools:** Axios, Visual Studio Code, GitLab

## Installation
1. Klonen Sie das Repository:
   ```bash
   git clone https://gitlab.com/Dankpixel007/projektwoche-pw2i_02-ogd.git
   ```
2. Navigieren Sie in das Projektverzeichnis:
   ```bash
   cd projektwoche-pw2i_02-ogd
   ```
3. Installieren Sie die Abhängigkeiten:
   ```bash
   npm install
   ```
4. Starten Sie die Applikation:
   ```bash
   npm run start
   ```
5. Öffnen Sie Ihren Browser und navigieren Sie zu `http://localhost:3001/`.

## Benutzung
Nach dem Start der Applikation können Sie verschiedene Diagramme und eine interaktive Karte betrachten:
- **Balkendiagramm:** Zeigt die Ausbildungslevel der Bevölkerung.
- **Kuchendiagramme:** Visualisieren die Daten nach Altersklassen und anderen Kategorien.
- **Interaktive Karte:** Zeigt spezifische Daten zu den einzelnen Kantonen an, wenn man darüber hovert.

## Funktionen
- **Drei Diagramme:** Ein Balkendiagramm und zwei Kuchendiagramme zur Visualisierung verschiedener Aspekte der Bildungsdaten.
- **Interaktive Karte:** SVG-Karte der Schweiz mit Tooltips für detaillierte Kantonsdaten.
- **Filter:** Ermöglicht das Anpassen der angezeigten Daten in den Diagrammen.

## Architektur
Die Architektur der Webanwendung umfasst:
- **Backend:** Implementiert mit Node.js und Express, lädt Daten von OpenGovernmentData und stellt sie als API bereit.
- **Frontend:** Erstellt mit HTML, CSS und JavaScript, verwendet Chart.js zur Diagrammerstellung.

## Testfälle
### Konstruktive Tests
1. **Hover-Effekt auf der Karte:** Tooltip erscheint mit Kantonnamen und Erwerbstätigkeit.
2. **Seitenaufbau:** Alle Diagramme werden korrekt angezeigt.
3. **Legendenklick im Balkendiagramm:** Daten werden ein- oder ausgeblendet.
4. **Checkbox im Kuchendiagramm:** Daten werden nach Altersklasse sortiert dargestellt.

### Destruktive Tests
1. **Leeres Balkendiagramm:** Diagramm wird leer, wenn alle Daten ausgeblendet sind.
2. **Checkbox im Kuchendiagramm wiederholt anklicken:** Diagramm wird korrekt zurückgesetzt und neu sortiert.
3. **Tooltip an Kantonsgrenzen:** Nur ein Tooltip wird angezeigt.

## Quellen
- **Daten:** [opendata.swiss](https://www.opendata.swiss)
- **Bilder:** 
  - Titelbild: [liip.ch](https://www.liip.ch/de/work/projects/opendata)
  - Navigationslogo: [bar.admin.ch](https://www.bar.admin.ch/bar/de/home/recherche/suchen/suchmaschinen-portale/open-government-data.html)
  - Titelbild Webseite: [pexels.com](https://www.pexels.com/photo/white-clouds-among-mountains-20019741/)

## Screenshots
![UML-Diagramm](Abbildung1.png)
![Karte mit Tooltip](Abbildung2.png)
![Kuchendiagramm mit Filterfunktion](Abbildung3.png)
![Filter im Balkendiagramm](Abbildung4.png)
![Architektur](Abbildung5.png)
![Planung](Abbildung6.png)
![Wireframes](Abbildung7.png)

