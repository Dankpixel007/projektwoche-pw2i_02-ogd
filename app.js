import express from 'express';
import { router } from './backend/router.js';
import dotenv from 'dotenv';

dotenv.config({ path: `.env.${process.env.NODE_ENV || 'development'}` });
console.log('Environment:', process.env.NODE_ENV);


const app = express();


app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

app.use(express.static('frontend'));
app.use(express.json());


app.use('/api', router);

export default app;
