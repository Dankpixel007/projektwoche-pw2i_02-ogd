import { KantonStatistik } from '../backend/model.js';
import app from '../app.js';
import supertest from 'supertest';
import * as dbUtil from './db.util.js';
import { insertKantonData } from './db.util.js';

const KANTON_BASE_PATH = '/api/kantone-database';

async function checkThatDbContains(expectedKantone) {
  const actualKantone = await dbUtil.getAllKantone();
  if (!Array.isArray(actualKantone)) {
    throw new Error('Die abgerufenen Kantondaten sind kein Array');
  }
  expect(actualKantone.length).toBe(expectedKantone.length);
  for (let i = 0; i < actualKantone.length; i++) {
    expect(actualKantone[i]).toMatchObject(expectedKantone[i]);
  }
}

async function checkThatResponseContains(response, expectedKantone) {
  let actualKantone = response.body;
  if (!Array.isArray(actualKantone)) {
    actualKantone = [actualKantone];
  }
  expect(actualKantone.length).toBe(expectedKantone.length);
  for (let i = 0; i < actualKantone.length; i++) {
    expect(actualKantone[i]).toMatchObject(expectedKantone[i]);
  }
}

describe('Daten werden eingefügt', () => {
  const api = supertest(app);

  beforeAll(async () => {
    await dbUtil.connectToDb();
  });
  beforeEach(async () => {
    await KantonStatistik.deleteMany({});
  });

  afterAll(async () => {
    await dbUtil.closeDbConnection();
  });

  afterEach(async () => {
    await dbUtil.clearAllCollections();
  });

  test('KantonStatistik-Datenbank wird geleert', async () => {
    // Given
    const kantonData = [
      { name: 'Genf', erwerbstätige: 30000, erwerblose: 1200 },
    ];
    await insertKantonData(kantonData);

    // When
    await dbUtil.clearAllCollections();
    const result = await KantonStatistik.find({});

    // Then
    expect(result.length).toBe(0);
  });

  test('Fehler beim Hinzufügen von ungültigen KantonStatistik-Daten', async () => {
    // Given
    const invalidData = { invalidField: 'Fehlerhafte Daten' };

    // When/Then
    await expect(KantonStatistik.create(invalidData)).rejects.toThrow();
  });

  test('Bereinigte Daten werden erfolgreich in MongoDB gespeichert', async () => {
    // Given
    const kantonData = {
      name: 'Zürich',
      ausbildungsstand: {
        keineAusbildungAbgeschlossen: 11719,
        sekundarstufeI: 104377,
        sekundarstufeII: 158536,
        tertiärstufe: 47098,
      },
      erwerbstätige: 132339,
      erwerblose: 5253,
      arbeitsniveau: {
        nichtZuteilbare: 39210,
        ungelernt: 18284,
      },
      alterskategorie: {
        '15_24': 18325,
        '25_34': 31751,
      },
    };

    // When
    await dbUtil.addKantone([kantonData]);
    const savedData = await dbUtil.getAllKantone();

    // Then
    expect(savedData.length).toBe(1);
    expect(savedData[0]).toMatchObject(kantonData);
  });
  test('KantonStatistik-Daten werden erfolgreich hinzugefügt und abgerufen', async () => {
    // Given
    const kantonData = [
      { name: 'Zürich', erwerbstätige: 50000, erwerblose: 2000 },
      { name: 'Bern', erwerbstätige: 45000, erwerblose: 1500 },
    ];
    await insertKantonData(kantonData);

    // When
    const result = await KantonStatistik.find({});

    // Then
    expect(result.length).toBe(2);
    expect(result[0]).toMatchObject(kantonData[0]);
    expect(result[1]).toMatchObject(kantonData[1]);
  });
});

describe('OGD Datenbank-Integrationstests', () => {
  const api = supertest(app);
  beforeAll(async () => {
    await dbUtil.connectToDb();
  });

  beforeEach(async () => {
    await KantonStatistik.deleteMany({});
  });

  afterAll(async () => {
    await dbUtil.closeDbConnection();
  });

  afterEach(async () => {
    await dbUtil.clearAllCollections();
  });

  test('Kriegt keine Kantone von einer leeren Datenbank', async () => {
    // Given

    // When
    const response = await api.get(KANTON_BASE_PATH);

    // Then
    expect(response.status).toBe(200);
    checkThatResponseContains(response, []);
  });
  test('Kriegt gespeicherte Daten korrekt zurück', async () => {
    // Given
    const expectedKantone = [
      {
        name: 'Canton du Valais',
        ausbildungsstand: {
          keineAusbildungAbgeschlossen: 11719,
          sekundarstufeI: 104377,
          sekundarstufeII: 158536,
          tertiärstufe: 47098,
        },
        erwerbstätige: 132339,
        erwerblose: 5253,
        arbeitsniveau: {
          nichtZuteilbare: 39210,
          ungelernt: 18284,
        },
        alterskategorie: {
          '15_24': 18325,
          '25_34': 31751,
        },
      },
    ];
    await dbUtil.addKantone(expectedKantone);

    // When
    const response = await api.get(KANTON_BASE_PATH);

    // Then
    expect(response.status).toBe(200);
    checkThatResponseContains(response, expectedKantone);
  });
  test('fehler beim hinzufügen ungültiger daten', async () => {
    // given
    const invalidKanton = {
      name: '',
      ausbildungsstand: {
        keineAusbildungAbgeschlossen: -1,
      },
    };

    // when
    const response = await api.post(KANTON_BASE_PATH).send(invalidKanton);

    // then
    expect(response.status).toBe(404);
  });

  test('nicht existierenden kanton abrufen', async () => {
    // given
    // keine datenbankeinträge vorhanden

    // when
    const response = await api.get(`${KANTON_BASE_PATH}?name=nonexistent`);

    // then
    expect(response.status).toBe(404);
    expect(response.body.error).toBe('kanton nicht gefunden');
  });
});

describe('Datenkombinationtest ', () => {
  beforeAll(async () => {
    await dbUtil.connectToDb();
  });
  beforeEach(async () => {
    await KantonStatistik.deleteMany({});
  });

  afterAll(async () => {
    await dbUtil.closeDbConnection();
  });

  afterEach(async () => {
    await dbUtil.clearAllCollections();
  });

  test('Datenbank enthält erwartete Kantondaten', async () => {
    //Given
    const expectedKantone = [
      { name: 'Zürich', erwerbstätige: 50000 },
      { name: 'Bern', erwerbstätige: 45000 },
    ];
    //when
    await dbUtil.addKantone(expectedKantone);
    //then
    await checkThatDbContains(expectedKantone);
  });
});
