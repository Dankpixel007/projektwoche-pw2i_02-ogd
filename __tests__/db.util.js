import mongoose from "mongoose";
import { User, KantonStatistik, Review } from "../backend/model.js";
import dotenv from "dotenv";

dotenv.config({ path: `.env.${process.env.NODE_ENV || "development"}` });
const mongoURI = process.env.MONGODB_URI;
async function connectToDb() {
  if (!process.env.MONGODB_URI) {
    throw new Error("MONGODB_URI is not defined. Check your .env file.");
  }
  await mongoose.connect(process.env.MONGODB_URI);
}

async function closeDbConnection() {
  await mongoose.connection.close();
}

async function clearAllCollections() {
  await User.deleteMany({});
  await KantonStatistik.deleteMany({});
  await Review.deleteMany({});
}

const createUser = async (userData) => {
  try {
    let user = await User.findOne({
      email: userData.email,
      name: userData.name,
    });
    if (!user) {
      user = await User.create(userData);
    }
    return user;
  } catch (error) {
    console.error("Error creating user:", error.message);
    throw error;
  }
};

const createReview = async (reviewData) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(reviewData.user)) {
      throw new Error("Invalid user ObjectId in reviewData");
    }
    const review = await Review.create(reviewData);
    return review;
  } catch (error) {
    console.error("Error creating review:", error.message);
    throw error;
  }
};

// Delete a Review
const deleteReview = async (reviewId) => {
  try {
    const review = await Review.findByIdAndDelete(reviewId);
    if (!review) {
      console.warn("Review not found:", reviewId);
      return null;
    }
    return review;
  } catch (error) {
    console.error("Error deleting review:", error.message);
    throw error;
  }
};

// Edit a Review (Controller Logic)
const editReview = async (reviewId, updateData) => {
  try {
    // Fetch the review by ID
    const review = await Review.findById(reviewId);
    if (!review) {
      console.warn("Review not found:", reviewId);
      return null;
    }

    // Validate input data (same as controller)
    if (
      !updateData.text ||
      typeof updateData.text !== "string" ||
      !updateData.sterne ||
      typeof updateData.sterne !== "number"
    ) {
      throw new Error("Invalid input for review update");
    }

    // Apply updates
    review.text = updateData.text;
    review.sterne = updateData.sterne;

    // Save changes
    await review.save();

    return review;
  } catch (error) {
    console.error("Error updating review:", error.message);
    throw error;
  }
};

const addKantone = async (kantone) => {
  await KantonStatistik.insertMany(kantone);
};
async function getAllKantone() {
  return await KantonStatistik.find({}).lean();
}
async function insertKantonData(kantone) {
  await KantonStatistik.insertMany(kantone);
}

export {
  connectToDb,
  closeDbConnection,
  clearAllCollections,
  addKantone,
  getAllKantone,
  insertKantonData,
  createReview,
  deleteReview,
  editReview,
  createUser,
};
