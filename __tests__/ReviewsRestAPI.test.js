import request from "supertest";
import app from "../app.js";
import {
  connectToDb,
  closeDbConnection,
  clearAllCollections,
  createReview,
  deleteReview,
  editReview,
  createUser,
} from "./db.util.js";
import { User, Review } from "../backend/model.js";
import dotenv from "dotenv";
import mongoose from "mongoose";

dotenv.config();

describe("Tests the Review API Endpoint", () => {
  const api = request(app);
  let mockUserId;
  let mockReviewId;

  // Setup and Teardown**
  beforeAll(async () => {
    await connectToDb();
  });

  afterAll(async () => {
    await clearAllCollections();
    await closeDbConnection();
  });

  beforeEach(async () => {
    await clearAllCollections();
    const user = await createUser({
      name: "Test User",
      email: "test@example.com",
    });
    mockUserId = user._id;

    const review = await createReview({
      text: "Test review text",
      sterne: 4,
      user: mockUserId,
      email: "test@example.com",
      diagram: "Diagram A",
    });
    mockReviewId = review._id;
  });

  test("Should edit review ", async () => {
    // given
    const existingReview = await Review.findById(mockReviewId.toString());
    expect(existingReview).not.toBeNull();

    // when
    await editReview(mockReviewId.toString(), {
      text: "Updated review text",
      sterne: 5,
    });

    const fetchedReview = await Review.findById(mockReviewId).lean();

    // then
    expect(fetchedReview).not.toBeNull();
    expect(fetchedReview.text).toBe("Updated review text");
    expect(fetchedReview.sterne).toBe(5);
  });

  test("Should return 400 for invalid review update data", async () => {
    // given
    const invalidReviewData = {
      text: 123,
      sterne: "five",
    };

    // when
    const res = await api
      .put(`/api/reviews/${mockReviewId}`)
      .send(invalidReviewData);

    // then
    expect(res.statusCode).toBe(400);
  });

  test("Should return 404 if review does not exist", async () => {
    // given
    const nonExistentId = new mongoose.Types.ObjectId();

    const validReviewData = {
      text: "Valid Text",
      sterne: 5,
    };

    // when
    const res = await api
      .put(`/api/reviews/${nonExistentId}`)
      .send(validReviewData);

    // then
    expect(res.statusCode).toBe(404);
    expect(res.text).toBe("Review not found");
  });

  test("Should create a new review", async () => {
    // given
    const reviewData = {
      text: "Excellent visualization!",
      sterne: 4,
      user: mockUserId,
      email: "test2@example.com",
      diagram: "Diagram A",
    };

    // when
    const res = await api.post("/api/reviews").send(reviewData);

    // then
    const createdReview = await Review.findById(res.body._id);
    expect(createdReview.text).toBe("Excellent visualization!");
  });

  test("Should delete a review", async () => {
    // given review created in setup exists

    // when
    await deleteReview(mockReviewId);

    // then
    const deletedReview = await Review.findById(mockReviewId);
    expect(deletedReview).toBeNull();
  });

  test("Should fetch all reviews", async () => {
    // given review created in setup exists

    // when
    const res = await api.get("/api/reviews");

    // then
    const userGroup = res.body[mockUserId.toString()];
    expect(userGroup).toBeDefined();
    expect(userGroup.user.name).toBe("Test User");
    expect(userGroup.user.email).toBe("test@example.com");
    expect(userGroup.reviews.length).toBeGreaterThanOrEqual(1);
  });

  test("Should fetch a review by ID", async () => {
    // given review created in setup exists

    // when
    const res = await api.get(`/api/reviews/${mockReviewId}`);

    // then
    expect(res.statusCode).toBe(200);
    expect(res.body.text).toBe("Test review text");
  });
});
