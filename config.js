import dotenv from "dotenv";
import { fileURLToPath } from "url";
import path from "path";

const pathToCurrentFile = fileURLToPath(import.meta.url);
const pathToCurrentFolder = path.dirname(pathToCurrentFile);

function loadConfig() {
  // Skip loading .env files in production
  if (process.env.NODE_ENV === "production") {
    console.log("Running in production mode. Skipping .env file loading.");
    return;
  }

  const envFile = `.env.${process.env.NODE_ENV}`;
  const envPath = path.join(pathToCurrentFolder, ".", envFile);

  const result = dotenv.config({ path: envPath });

  if (result.error) {
    console.error(`Error loading ${envFile}:`, result.error.message);
    process.exit(1);
  }

  console.log(`Environment variables loaded from ${envFile}`);
}

// Load configuration on import
loadConfig();

export default {
  mongoURI: process.env.MONGODB_URI || "mongodb://localhost:27017/ogdprod",
  port: process.env.PORT || 3001,
};
