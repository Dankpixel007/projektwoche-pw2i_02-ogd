import { KantonStatistik } from '../model.js';
import {
  getEmployedByAgeGroup,
  getTotalEmploymentData,
  getEducationLevelData,
  getEducationLevel,
  getAlleKantoneDaten,
} from './ogdDataUtil.js';

export async function combineAndSaveData() {
  try {
    const dataArray = await getAlleKantoneDaten();

    // von allen die gefilterten Sachen zurück
    const ageGroupData = getEmployedByAgeGroup(dataArray);
    const employmentData = getTotalEmploymentData(dataArray);
    const educationLevelData = getEducationLevelData(dataArray);
    const workLevelData = getEducationLevel(dataArray);

    // Daten kombinieren und in die Datenbank speichern
    await saveCompleteDataToDatabase(
      ageGroupData,
      employmentData,
      educationLevelData,
      workLevelData
    );
    console.log('erfolgreich geladen');
  } catch (error) {
    console.error('Fehler beim Kombinieren oder Speichern der Daten:', error);
  }
}

async function saveCompleteDataToDatabase(
  ageGroupData,
  employmentData,
  educationLevelData,
  workLevelData
) {
  try {
    for (const canton in ageGroupData) {
      const ageGroups = ageGroupData[canton];
      const provinceData = employmentData.find((prov) => prov.area === canton);

      // Überprüfen, ob der Eintrag für das gleiche Gebiet existiert
      const existingEntry = await KantonStatistik.findOne({ name: canton });

      // Fülle die Schema-Objekte
      const fullData = {
        name: canton,
        erwerbstätige: provinceData.data.employed.total,
        erwerblose: provinceData.data.unemployed.total,

        // Alterskategorien füllen
        alterskategorie: {
          '15_24': ageGroups['15-24 Jahre'] || 0,
          '25_34': ageGroups['25-34 Jahre'] || 0,
          '35_44': ageGroups['35-44 Jahre'] || 0,
          '45_54': ageGroups['45-54 Jahre'] || 0,
          '55_64': ageGroups['55-64 Jahre'] || 0,
          '65_plus': ageGroups['65 Jahre und mehr'] || 0,
        },

        ausbildungsstand: {
          keineAusbildungAbgeschlossen:
            workLevelData[canton]?.['Keine Ausbildung abgeschlossen'] || 0,
          sekundarstufeI: workLevelData[canton]?.['Sekundarstufe I'] || 0,
          sekundarstufeII: workLevelData[canton]?.['Sekundarstufe II'] || 0,
          tertiärstufe: workLevelData[canton]?.['Tertiärstufe'] || 0,
        },

        arbeitsniveau: {
          nichtZuteilbare:
            educationLevelData.educationLevels[canton]?.[
              'Nicht zuteilbare Erwerbstätige'
            ]?.total || 0,
          ungelernt:
            educationLevelData.educationLevels[canton]?.[
              'Ungelernte Angestellte und Arbeiter'
            ]?.total || 0,
          qualifiziertManuell:
            educationLevelData.educationLevels[canton]?.[
              'Qualifizierte manuelle Berufe'
            ]?.total || 0,
          qualifiziertNichtManuell:
            educationLevelData.educationLevels[canton]?.[
              'Qualifizierte nicht-manuelle Berufe'
            ]?.total || 0,
          intermediaere:
            educationLevelData.educationLevels[canton]?.['Intermediäre Berufe']
              ?.total || 0,
          akademische:
            educationLevelData.educationLevels[canton]?.[
              'Akademische Berufe und oberes Kader'
            ]?.total || 0,
          andereSelbst:
            educationLevelData.educationLevels[canton]?.['Andere Selbständige']
              ?.total || 0,
          freieBerufe:
            educationLevelData.educationLevels[canton]?.['Freie Berufe']
              ?.total || 0,
          oberstesManagement:
            educationLevelData.educationLevels[canton]?.['Oberstes Management']
              ?.total || 0,
        },
      };
      // Speichern oder Aktualisieren
      if (existingEntry) {
        // Update des bestehenden Eintrags
        await KantonStatistik.updateOne(
          { _id: existingEntry._id },
          { $set: fullData }
        );
      } else {
        // Erstellen eines neuen Eintrags
        const newEntry = new KantonStatistik(fullData);
        await newEntry.save();
      }
    }

    console.log('Daten erfolgreich gespeichert');
  } catch (error) {
    console.error('Fehler beim Speichern der Daten:', error);
  }
}
