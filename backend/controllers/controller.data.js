import { KantonStatistik } from '../model.js';

export const getDataKantone = async (req, res) => {
  try {
    const { name } = req.query;

    if (name) {
      // Suche nach einem spezifischen Kanton basierend auf dem Namen
      const kanton = await KantonStatistik.findOne({ name });
      if (!kanton) {
        return res.status(404).json({ error: 'kanton nicht gefunden' });
      }
      return res.json(kanton);
    }

    const kantone = await KantonStatistik.find();
    res.json(kantone);
  } catch (error) {
    console.error('Fehler beim Abrufen der Kantonsdaten:', error);
    res.status(500).send('serverfehler');
  }
};
