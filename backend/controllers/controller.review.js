import { Review, User } from "../model.js";

// GET all reviews grouped by user
export async function getReviews(req, res) {
  try {
    const reviews = await Review.find({}).populate("user", "name email").exec();

    // Group reviews by user
    const groupedReviews = reviews.reduce((acc, review) => {
      const userId = review.user._id.toString();
      if (!acc[userId]) {
        acc[userId] = {
          user: {
            name: review.user.name,
            email: review.user.email,
          },
          reviews: [],
        };
      }
      acc[userId].reviews.push({
        _id: review._id,
        text: review.text,
        sterne: review.sterne,
        diagram: review.diagram,
      });
      return acc;
    }, {});

    res.json(groupedReviews);
  } catch (error) {
    console.error("Error fetching reviews:", error);
    res.status(500).send("Error fetching reviews");
  }
}

export async function getSingleReview(req, res) {
  try {
    const { reviewId } = req.params;
    const review = await Review.findById(reviewId).populate(
      "user",
      "name email"
    );
    if (!review) {
      return res.status(404).send("Review not found");
    }
    res.json(review);
  } catch (error) {
    console.error("Error fetching review:", error);
    res.status(500).send("Error fetching review");
  }
}

// POST new review
export async function addReview(req, res) {
  try {
    const { text, sterne, user, email, diagram } = req.body;

    // Find user by email and name
    let userDoc = await User.findOne({ email, name: user });
    if (!userDoc) {
      // Create a new user if not found
      userDoc = new User({ name: user, email });
      await userDoc.save();
    }

    // Save the review
    const newReview = new Review({
      text,
      sterne,
      user: userDoc._id,
      diagram,
    });
    await newReview.save();

    res.status(201).json(newReview);
  } catch (error) {
    console.error("Error saving review:", error);
    res.status(500).send("Error saving review");
  }
}

// PUT edit review
export async function editReview(req, res) {
  try {
    const { text, sterne } = req.body;

    // Ensure text and stars are provided
    if (
      !text ||
      typeof text !== "string" ||
      !sterne ||
      typeof sterne !== "number"
    ) {
      return res.status(400).send("Invalid input for review update");
    }

    const review = await Review.findById(req.params.reviewId);
    if (!review) {
      return res.status(404).send("Review not found");
    }

    review.text = text;
    review.sterne = sterne;
    await review.save();

    res.json(review);
  } catch (error) {
    console.error("Error saving review:", error);
    res.status(500).send("Error saving review");
  }
}

export async function deleteReview(req, res) {
  try {
    const { reviewId } = req.params;

    // Find the review to delete
    const review = await Review.findById(reviewId);
    if (!review) {
      return res.status(404).send("Review not found");
    }

    // Delete the review
    await Review.findByIdAndDelete(reviewId);

    // Check if the user has any remaining reviews
    const remainingReviews = await Review.find({ user: review.user });
    if (remainingReviews.length === 0) {
      // If no reviews are left, delete the user
      await User.findByIdAndDelete(review.user);
    }

    res.status(204).send();
  } catch (error) {
    console.error("Error deleting review:", error);
    res.status(500).send("Error deleting review");
  }
}
