import axios from 'axios';
export async function getPieCheckbox(req, res) {
  try {
    const pieCheckbox = getEmployedByAgeGroup(dataArray);
    res.json(pieCheckbox);
  } catch (error) {
    res.status(500).send('Failed to fetch or process data');
  }
}
// List of URLs to fetch data from
const urls = [
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202889/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/121886/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/121906/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202943/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202875/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/121043/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202806/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202957/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202813/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/121890/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/121051/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/121902/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202903/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202834/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202939/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202964/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202809/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/121910/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/121882/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202861/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/121031/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202827/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202831/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/121862/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202848/appendix',
  'https://dam-api.bfs.admin.ch/hub/api/dam/assets/202792/appendix',
];

export async function getAlleKantoneDaten() {
  try {
    let dataArray = [];

    // For each URL in the array
    for (const url of urls) {
      const response = await axios.get(url);
      const data = response.data;
      dataArray.push(data);
    }

    // Return the array
    return dataArray;
  } catch (error) {
    console.error('Error fetching data:', error);
    throw error;
  }
}
export function getEmployedByAgeGroup(dataArray) {
  // Verwende ein Objekt
  const ageGroupDataByCanton = {};

  dataArray.forEach((data) => {
    const cantonName = data.dataset[0].area; // Annahme: Der Kantonsname ist hier

    // Initialisiere die Altersgruppendaten für den Kanton, falls nicht vorhanden
    if (!ageGroupDataByCanton[cantonName]) {
      ageGroupDataByCanton[cantonName] = {
        '15-24 Jahre': 0,
        '25-34 Jahre': 0,
        '35-44 Jahre': 0,
        '45-54 Jahre': 0,
        '55-64 Jahre': 0,
        '65 Jahre und mehr': 0,
      };
    }

    data.dataset.forEach((record) => {
      // Überprüfe, ob das Worksheet den Namen 'Erwerbssituation' hat
      if (record.worksheet_name === 'Erwerbssituation') {
        record.worksheet_dataset.forEach((entry) => {
          if (
            entry.row_header.length === 2 && // Zwei Zeilenüberschriften
            entry.row_header[0] === 'Total' && // Erste Zeilenüberschrift ist 'Total'
            entry.row_header[1] === 'Erwerbstätige' && // Zweite Zeilenüberschrift ist 'Erwerbstätige'
            entry.column_header.includes('Total') // Spaltenüberschrift enthält 'Total'
          ) {
            // Altersgruppe ermitteln
            const ageGroup = determineAgeGroup(entry.column_header);
            if (ageGroup) {
              // Erhöhe die Anzahl für die spezifische Altersgruppe
              ageGroupDataByCanton[cantonName][ageGroup] += parseInt(
                entry.measurement
              );
            }
          }
        });
      }
    });
  });

  // Rückgabe der Altersgruppendaten pro Kanton
  return ageGroupDataByCanton;
}

function determineAgeGroup(columnHeader) {
  switch (true) {
    case columnHeader.includes('15-24 Jahre'):
      return '15-24 Jahre';
    case columnHeader.includes('25-34 Jahre'):
      return '25-34 Jahre';
    case columnHeader.includes('35-44 Jahre'):
      return '35-44 Jahre';
    case columnHeader.includes('45-54 Jahre'):
      return '45-54 Jahre';
    case columnHeader.includes('55-64 Jahre'):
      return '55-64 Jahre';
    case columnHeader.includes('65 Jahre und mehr'):
      return '65 Jahre und mehr';
    default:
      return null; // Rückgabe null für ungültige Altersgruppen
  }
}

export async function getKantone(req, res) {
  try {
    const employmentData = getTotalEmploymentData(dataArray);
    res.json(employmentData);
  } catch (error) {
    res.status(500).send('Failed to fetch or process data');
  }
}

export function getTotalEmploymentData(dataArray) {
  let employmentData = [];

  // Iterate over each dataset entry
  dataArray.forEach((data) => {
    let provinceData = {
      area: data.dataset[0].area, // Assuming all entries are for the same area
      period: data.period,
      data: {},
    };

    data.dataset.forEach((entry) => {
      entry.worksheet_dataset.forEach((dataPoint) => {
        if (
          dataPoint.column_header.includes('15 Jahre und mehr') &&
          dataPoint.column_header.includes('Total')
        ) {
          if (
            dataPoint.row_header.includes('Erwerbstätige') &&
            dataPoint.row_header.includes('Total') &&
            dataPoint.row_header.length === 2
          ) {
            provinceData.data.employed = {
              total: dataPoint.measurement,
              description: 'Total employed individuals',
            };
          }
          // Check for unemployed data
          else if (
            dataPoint.row_header.includes('Erwerbslose') &&
            dataPoint.row_header.includes('Total') &&
            dataPoint.row_header.length === 2
          ) {
            provinceData.data.unemployed = {
              total: dataPoint.measurement,
              description: 'Total unemployed individuals',
            };
          }
        }
      });
    });

    employmentData.push(provinceData);
  });

  return employmentData;
}
export async function getEducationData(req, res) {
  try {
    const educationData = getEducationLevelData(dataArray);
    res.json(educationData);
  } catch (error) {
    res.status(500).send('Failed to fetch or process data');
  }
}

export function getEducationLevelData(dataArray) {
  const educationLevelsToInclude = [
    '01 Oberstes Management',
    '02 Freie Berufe',
    '03 Andere Selbständige',
    '04 Akademische Berufe und oberes Kader',
    '05 Intermediäre Berufe',
    '06 Qualifizierte nicht-manuelle Berufe',
    '07 Qualifizierte manuelle Berufe',
    '08 Ungelernte Angestellte und Arbeiter',
    '09 Nicht zuteilbare Erwerbstätige',
  ];

  let aggregatedData = {};

  try {
    dataArray.forEach((data) => {
      if (!data.dataset) {
        console.error('No dataset found in data:', data);
        return;
      }

      // Der Kantonsname
      const cantonName = data.dataset[0].area;

      // Initialisiere die Datenstruktur für den Kanton, falls noch nicht vorhanden
      if (!aggregatedData[cantonName]) {
        aggregatedData[cantonName] = {};
      }

      data.dataset.forEach((worksheet) => {
        // Überprüfe, ob der Worksheet-Name "Sozioprofessionelle Kategorie" ist
        if (worksheet.worksheet_name === 'Sozioprofessionelle Kategorie') {
          if (!worksheet.worksheet_dataset) {
            console.error('No dataset found in worksheet:', worksheet);
            return;
          }

          worksheet.worksheet_dataset.forEach((dataPoint) => {
            if (
              dataPoint.column_header.includes('Total') &&
              dataPoint.row_header.includes('Total')
            ) {
              const educationKey =
                dataPoint.row_header[dataPoint.row_header.length - 1];
              if (educationLevelsToInclude.includes(educationKey)) {
                const description = `Total individuals in the ${educationKey} category`;

                // Initialisiere die Struktur für die Bildungsstufen, falls nicht vorhanden
                if (!aggregatedData[cantonName][educationKey]) {
                  aggregatedData[cantonName][educationKey] = {
                    total: 0,
                    description: description,
                  };
                }

                // Summe der Werte aggregieren
                aggregatedData[cantonName][educationKey].total += parseInt(
                  dataPoint.measurement
                );
              }
            }
          });
        }
      });
    });

    // Entferne die Zahlen von den Schlüsseln und formatiere die Daten
    const cleanedData = {};
    Object.keys(aggregatedData).forEach((canton) => {
      cleanedData[canton] = {};
      Object.keys(aggregatedData[canton]).forEach((key) => {
        const cleanedKey = key.replace(/^\d+\s/, ''); // Entferne führende Zahlen und Leerzeichen
        cleanedData[canton][cleanedKey] = aggregatedData[canton][key];
      });
    });

    return {
      area: 'Switzerland',
      period: '2000',
      educationLevels: cleanedData,
    };
  } catch (error) {
    console.error('Failed to fetch or process data', error);
    return {
      area: 'Switzerland',
      period: '2000',
      educationLevels: {},
      error: 'Failed to fetch or process data',
    };
  }
}
export async function getBlockChartData(req, res) {
  try {
    const blockData = getEducationLevel(dataArray);
    res.json(blockData);
  } catch (error) {
    res.status(500).send('Failed to fetch or process data');
  }
}

export function getEducationLevel(dataArray) {
  let educationDataPerCanton = {};

  dataArray.forEach((data) => {
    const cantonName = data.dataset[0].area;

    // Initialisiere die Struktur für jeden Kanton, falls noch nicht vorhanden
    if (!educationDataPerCanton[cantonName]) {
      educationDataPerCanton[cantonName] = {
        'Keine Ausbildung abgeschlossen': 0,
        'Sekundarstufe I': 0,
        'Sekundarstufe II': 0,
        Tertiärstufe: 0,
      };
    }

    data.dataset.forEach((record) => {
      record.worksheet_dataset.forEach((entry) => {
        if (
          entry.row_header.includes('Total') &&
          entry.row_header.length === 1
        ) {
          // Ausbildungsniveau ermitteln
          const educationLevel = determineEducationLevel(entry.column_header);

          // Wenn das Ausbildungsniveau erkannt wird, aggregiere die Werte für den Kanton
          if (educationLevel) {
            educationDataPerCanton[cantonName][educationLevel] += parseInt(
              entry.measurement
            );
          }
        }
      });
    });
  });

  return educationDataPerCanton;
}

function determineEducationLevel(columnHeader) {
  // Ermitteln welches Level und zurückgeben
  if (columnHeader.includes('Keine Ausbildung abgeschlossen')) {
    return 'Keine Ausbildung abgeschlossen';
  } else if (columnHeader.includes('Sekundarstufe I')) {
    return 'Sekundarstufe I';
  } else if (columnHeader.includes('Sekundarstufe II')) {
    return 'Sekundarstufe II';
  } else if (columnHeader.includes('Tertiärstufe')) {
    return 'Tertiärstufe';
  } else {
    return null;
  }
}
