import express from 'express';

import { getDataKantone } from './controllers/controller.data.js';
import { getReviews, addReview, getSingleReview, editReview, deleteReview } from './controllers/controller.review.js';

const router = express.Router();

router.get('/kantone-database', getDataKantone);

// Review Routes
router.get('/reviews', getReviews);
router.post('/reviews', addReview); 
router.get('/reviews/:reviewId', getSingleReview);
router.put('/reviews/:reviewId', editReview); 
router.delete('/reviews/:reviewId', deleteReview); 


export { router };
