import mongoose from 'mongoose';




// Schema for User
const userSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
  },
  { collection: 'users' }
);



// Schema für KantonStatistik
const kantonStatistikSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    ausbildungsstand: { type: Object, required: false },
    erwerbstätige: { type: Number, required: false },
    erwerblose: { type: Number, required: false },
    arbeitsniveau: { type: Object, required: false },
    alterskategorie: { type: Object, required: false },
  },
  { versionKey: false }
);

// Schema for Review
const reviewSchema = new mongoose.Schema(
  {
    text: { type: String, required: true },
    sterne: { type: Number, required: true },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }, 
    diagram: { type: String, required: true }, 
  },
  { collection: 'reviews' }
);


// Export the models
export const User = mongoose.model('User', userSchema);
export const KantonStatistik = mongoose.model(
  'KantonStatistik',
  kantonStatistikSchema
);
export const Review = mongoose.model('Review', reviewSchema);