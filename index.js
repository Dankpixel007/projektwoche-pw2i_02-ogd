import mongoose from "mongoose";
import app from "./app.js";
import { combineAndSaveData } from "./backend/controllers/controller.insert.js";
import config from "./config.js"; // Import your config module

// Connect to MongoDB
mongoose
  .connect(config.mongoURI, {})
  .then(() => {
    console.log(`Connected to MongoDB: ${config.mongoURI}`);
  })
  .catch((err) => {
    console.error("Error connecting to MongoDB:", err.message);
    process.exit(1); // Exit if connection fails
  });

// Start the server
app.listen(config.port, async () => {
  await combineAndSaveData();
  console.log(`Server is running at http://localhost:${config.port}`);
});
