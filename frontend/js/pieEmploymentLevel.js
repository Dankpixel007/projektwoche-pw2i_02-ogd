function createEmploymentLevelChart(educationData) {
  if (educationData) {
    const labels = Object.keys(educationData);
    const data = Object.values(educationData).map((level) => level);
    const backgroundColors = [
      // Farben für Balken
      'rgb(255,250,230,0.9)',
      'rgb(255,146,9,0.9)',
      'rgb(255,159,102,0.9)',
      'rgb(255,95,0,0.9)',
      'rgb(1, 19, 64, 0.9)',
      'rgb(43,52,153,0.9)',
      'rgb(120, 152, 197, 0.9)',
      'rgb(100,204,197,0.9)',
      'rgb(1,116,190,0.9)',
    ];

    const ctx = document.getElementById('educationPieChart').getContext('2d');

    const config = {
      type: 'pie',
      data: {
        labels: labels,
        datasets: [
          {
            label: 'Total Individuen',
            data: data,
            backgroundColor: backgroundColors,
          },
        ],
      },
      options: {
        responsive: true,
        plugins: {
          legend: {
            display: false, // Legende ausblenden
          },
          title: {
            display: false,
            text: 'Total Individuals by Education Level',
          },
        },
      },
    };

    const myPieChart = new Chart(ctx, config);
  }
}

export { createEmploymentLevelChart };
