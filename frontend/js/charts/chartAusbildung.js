const ID_CHART_COURSES = 'ausbildungChart';
let chartInstance;

const educationLevelLabels = {
  keineAusbildungAbgeschlossen: 'Keine Ausbildung abgeschlossen',
  sekundarstufeI: 'Sekundarstufe I',
  sekundarstufeII: 'Sekundarstufe II',
  tertiärstufe: 'Tertiärstufe',
};

export function createAusbildungChart(chartData) {
  const ctx = document.getElementById(ID_CHART_COURSES).getContext('2d');

  const labels = Object.keys(chartData).map((key) => educationLevelLabels[key]);
  const data = Object.values(chartData);

  // Array von Farben für die Balken
  const colors = [
    'rgb(255,159,102,0.9)',
    'rgb(255,95,0,0.9)',
    'rgb(1, 19, 64, 0.9)',
    'rgb(43,52,153,0.9)',
  ];

  if (!chartInstance) {
    chartInstance = new Chart(ctx, {
      type: 'bar',
      options: {
        animation: true,
        scales: {
          x: {
            ticks: {
              color: 'rgba(0, 0, 0, 0.7)',
            },
            border: {
              display: false,
            },
            grid: {
              display: false,
            },
          },
          y: {
            ticks: {
              color: 'rgba(0, 0, 0, 0.7)',
            },
            border: {
              color: 'rgba(0, 0, 0, 0.2)',
            },
            grid: {
              color: 'rgba(0, 0, 0, 0.2)',
            },
          },
        },
        plugins: {
          legend: {
            display: false,
          },
          title: {
            display: false,
            text: 'Anzahl der Arbeitenden',
            color: 'rgba(0, 0, 0, 0.7)',
            font: {
              size: 20,
            },
          },
        },
      },
      data: {
        labels: labels,
        datasets: [
          {
            label: 'Anzahl der Arbeitenden',
            data: data,
            backgroundColor: colors,
            borderColor: colors.map((color) => color.replace('0.5', '1')),
            borderWidth: 1,
          },
        ],
      },
    });
  } else {
    chartInstance.data.labels = labels;
    chartInstance.data.datasets[0].data = data;
    chartInstance.update();
  }
}
