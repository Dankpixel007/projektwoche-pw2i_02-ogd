function createEmploymentAgeGroupChart(pieData) {
  const ageGroupLabels = {
    '15_24': '15-24 Jahre',
    '25_34': '25-34 Jahre',
    '35_44': '35-44 Jahre',
    '45_54': '45-54 Jahre',
    '55_64': '55-64 Jahre',
    '65_plus': '65+ Jahre',
  };

  if (pieData) {
    // Initialize
    const ctx = document
      .getElementById('KuchendiagrammErwbNachAlter')
      .getContext('2d');

    let myPieChart;

    function updateChart() {
      const selectedAgeGroups = Array.from(
        document.querySelectorAll('input[type="checkbox"]:checked')
      ).map((checkbox) => checkbox.id);

      const labels = selectedAgeGroups.map((group) => ageGroupLabels[group]);
      const data = selectedAgeGroups.map((group) => pieData[group]);

      if (myPieChart) {
        myPieChart.destroy();
      }

      myPieChart = new Chart(ctx, {
        type: 'pie',
        data: {
          labels: labels,
          datasets: [
            {
              label: 'Erwerbstätige nach Altersgruppe',
              data: data,
              backgroundColor: [
                'rgb(255,250,230,0.9)',
                'rgb(255,159,102,0.9)',
                'rgb(255,95,0,0.9)',
                'rgb(1, 19, 64, 0.9)',
                'rgb(43,52,153,0.9)',
                'rgb(255,146,9,0.9)',
              ],
            },
          ],
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              display: false,
            },
            title: {
              display: false,
              text: 'Erwerbstätige nach Altersgruppe',
            },
          },
        },
      });
    }

    updateChart();

    document.querySelectorAll('input[type="checkbox"]').forEach((checkbox) => {
      checkbox.addEventListener('change', updateChart);
    });
  }
}

export { createEmploymentAgeGroupChart };
