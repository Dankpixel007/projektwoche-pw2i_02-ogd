let selectedRating = 0;
let editingReviewId = null;
let reviews = [];

function initializeRatingSystem() {
  const submitBtn = document.getElementById("submit-review-btn");
  const saveEditBtn = document.getElementById("save-edit-btn");

  submitBtn.addEventListener("click", submitReview);
  saveEditBtn.addEventListener("click", saveEditReview);

  initializeStarRating(); 

  
  fetchReviews();
}

// Initialize the star rating subsystem
function initializeStarRating() {
  const stars = document.querySelectorAll("#star-rating .star");

  stars.forEach((star) => {
    // Click to select a rating
    star.addEventListener("click", function () {
      selectedRating = parseInt(this.dataset.value);
      updateStarColors(); 
    });

    star.addEventListener("mouseover", function () {
      updateStarColors(parseInt(this.dataset.value)); // highlights all the stars up to the hovered star
    });

    // Reset hover effect when the mouse leaves
    star.addEventListener("mouseout", function () {
      updateStarColors(); 
    });
  });

  updateStarColors(); 
}

// Update star colors based on the current selection or hover state
function updateStarColors(hoveredRating = 0) {
  const stars = document.querySelectorAll("#star-rating .star");

  stars.forEach((star) => {
    const value = parseInt(star.dataset.value);

    star.classList.toggle("selected", value <= (hoveredRating || selectedRating));
  });
}

// Fetch all reviews 
async function fetchReviews() {
  try {
    const response = await axios.get("/api/reviews");
    displayReviews(response.data);
    reviews = response.data;
  } catch (error) {
    console.error("Error fetching reviews:", error);
  }
}

// displayReviews function
function displayReviews(groupedReviews) {
  const reviewsContainer = document.getElementById('reviews-container');
  if (!reviewsContainer) {
    console.error('reviewsContainer not found');
    return;
  }

  reviewsContainer.innerHTML = '';

  Object.values(groupedReviews).forEach((group) => {
    const userSection = document.createElement('div');
    userSection.classList.add('user-group');

    // User info
    const userInfo = document.createElement('h3');
    userInfo.textContent = `${group.user.name} (${group.user.email})`;
    userSection.appendChild(userInfo);

    // Add reviews for the user
    group.reviews.forEach((review) => { 
      const reviewElement = document.createElement('div');
      reviewElement.classList.add('review');

      // Stars for rating
      let starsHTML = '';
      for (let i = 1; i <= 5; i++) {
        starsHTML += `<span class="star ${i <= review.sterne ? 'selected' : ''}">★</span>`;
      }

      reviewElement.innerHTML = `
        <h3>${review.diagram}</h3>
        <div class="rating">${starsHTML}</div>
        <p>${review.text}</p>
        <button class="edit" onclick="startEditReview('${review._id}')">Edit</button>
        <button onclick="deleteReview('${review._id}')">Delete</button>
      `;

      userSection.appendChild(reviewElement);
    });

    reviewsContainer.appendChild(userSection);
  });
}


function renderStarRating(rating) {
  let starsHtml = "";
  for (let i = 1; i <= 5; i++) {
    starsHtml += `<span class="star ${i <= rating ? "selected" : ""}">★</span>`;
  }
  return `<div class="star-rating">${starsHtml}</div>`;
}

// Submit a new review
async function submitReview() {
  const username = document.getElementById('username').value.trim();
  const email = document.getElementById('email').value.trim();
  const diagram = document.getElementById('diagram-selection').value;
  const text = document.getElementById('review-text').value.trim();

  if (!username || !email || !text || selectedRating === 0) {
    alert('Please fill in all fields and select a rating');
    return;
  }

  // regex for email validation
  const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  if (!emailPattern.test(email)) {
    alert('Invalid email format. Please enter a valid email address.');
    return;
  }

  try {

    // Check if the email is already associated with a different username
    for (const userId in reviews) {
      const userGroup = reviews[userId];
      if (userGroup.user.email === email && userGroup.user.name !== username) {
        alert(
          `The email "${email}" is already associated with the username "${userGroup.user.name}". Please use the same username or a different email.`
        );
        return;
      }
    }

    // Submit the review
    await axios.post('/api/reviews', {
      text,
      sterne: selectedRating,
      user: username,
      email,
      diagram,
    });

    fetchReviews(); // refresh to display the new review
    resetForm();
  } catch (error) {
    console.error('Error submitting review:', error);
  }
}

// Edit an existing review
window.startEditReview = async function startEditReview(reviewId) {
  if (!reviewId) {
    console.error("Review ID is undefined");
    return;
  }

  try {
    const response = await axios.get(`/api/reviews/${reviewId}`);
    const review = response.data;

    document.getElementById("username").value = review.user.name;
    document.getElementById("email").value = review.user.email;
    document.getElementById("review-text").value = review.text;
    selectedRating = review.sterne;

    document.querySelectorAll("#star-rating .star").forEach((star) => {
      star.classList.remove("selected");
      if (star.dataset.value <= selectedRating) {
        star.classList.add("selected");
      }
    });

    document.getElementById("submit-review-btn").style.display = "none";
    document.getElementById("save-edit-btn").style.display = "inline-block";
    editingReviewId = reviewId;
  } catch (error) {
    console.error("Error fetching review:", error);
  }
};

// Save edited review
async function saveEditReview() {
  if (!editingReviewId) {
    console.error("No review selected for editing");
    return;
  }

  const newText = document.getElementById("review-text").value;
  const newRating = selectedRating;

  try {
    await axios.put(`/api/reviews/${editingReviewId}`, {
      text: newText,
      sterne: newRating,
    });

    fetchReviews(); // Refresh reviews to show the updated review
    resetForm();
  } catch (error) {
    console.error("Error saving review:", error);
  }
}

// Reset the form to clean state
function resetForm() {
  document.getElementById("username").value = "";
  document.getElementById("email").value = "";
  document.getElementById("review-text").value = "";
  selectedRating = 0;
  document.querySelectorAll("#star-rating .star").forEach((star) => star.classList.remove("selected"));
  document.getElementById("submit-review-btn").style.display = "inline-block";
  document.getElementById("save-edit-btn").style.display = "none";
  editingReviewId = null;
}

// Delete a review
window.deleteReview = async function deleteReview(reviewId) {
  if (!reviewId) {
    console.error("Review ID is undefined");
    return;
  }

  try {
    await axios.delete(`/api/reviews/${reviewId}`);
    fetchReviews(); 
  } catch (error) {
    console.error("Error deleting review:", error);
  }
};


initializeRatingSystem();
