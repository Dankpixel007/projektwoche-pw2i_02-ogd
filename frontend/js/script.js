import { createAusbildungChart } from './charts/chartAusbildung.js';
import { createEmploymentAgeGroupChart } from './charts/pieEmploymentAgeGroup.js';
import { createEmploymentLevelChart } from './pieEmploymentLevel.js';
import { getData } from './getDataService.js';

const cantonMap = {
  tg: 'Kanton Thurgau',
  vs: 'Canton du Valais',
  vd: 'Canton de Vaud',
  zg: 'Kanton Zug',
  ur: 'Kanton Uri',
  ti: 'Cantone del Ticino',
  sg: 'Kanton St. Gallen',
  so: 'Kanton Solothurn',
  sz: 'Kanton Schwyz',
  sh: 'Kanton Schaffhausen',
  lu: 'Kanton Luzern',
  ow: 'Kanton Obwalden',
  ne: 'Canton de Neuchâtel',
  gr: 'Kanton Graubünden',
  ju: 'Canton du Jura',
  nw: 'Kanton Nidwalden',
  bs: 'Kanton Basel-Stadt',
  ge: 'Canton de Genève',
  fr: 'Canton de Fribourg',
  be: 'Kanton Bern',
  gl: 'Kanton Glarus',
  ar: 'Kanton Appenzell A.Rh.',
  ai: 'Kanton Appenzell I.Rh.',
  bl: 'Kanton Basel-Landschaft',
  ag: 'Kanton Aargau',
  zh: 'Kanton Zürich',
};

let initialData = [];
let kantoneData = [];
let aggregatedAgeGroupData = {};
let aggregatedEmploymentLevelData = {};
let aggregatedEducationData = {};
let previousactivecanton = null;

async function initializeCharts() {
  const data = await getData('/api/kantone-database');
  if (data) {
    initialData = data;
    kantoneData = data;

    aggregatedAgeGroupData = aggregateAgeGroupData(initialData);
    createEmploymentAgeGroupChart(aggregatedAgeGroupData);

    aggregatedEmploymentLevelData = aggregateEmploymentLevelData(initialData);
    createEmploymentLevelChart(aggregatedEmploymentLevelData);

    aggregatedEducationData = aggregateEducationData(initialData);
    createAusbildungChart(aggregatedEducationData);

    // Initialisiere die Karteninteraktionen
    initializeMapInteractions();
  }
}

function initializeMapInteractions() {
  // Inject SVG and initialize map events
  const elMappCh = document.getElementById('ch-map');
  SVGInject.setOptions({ makeIdsUnique: false });

  SVGInject(elMappCh).then(() => {
    const totalMap = document.querySelectorAll('g');
    totalMap.forEach((element) => {
      element.classList.add('canton');
      element.addEventListener('mouseenter', showTooltip);
      element.addEventListener('mousemove', moveTooltip);
      element.addEventListener('mouseleave', hideTooltip);
    });

    Object.keys(cantonMap).forEach((id) => {
      const cantonElement = document.getElementById(id);
      if (cantonElement) {
        cantonElement.addEventListener('mouseenter', () =>
          updatetooltip(cantonElement)
        );
      }
    });

    const mapElement = document.getElementById('map-container');
    if (mapElement) {
      mapElement.addEventListener('mouseleave', hideTooltip);
    }
  });
}

function updatetooltip(cantonElement) {
  const tooltip = document.getElementById('tooltip-map');
  const cantonId = cantonElement.id;
  const cantonName = cantonMap[cantonId];
  const cantonData = kantoneData.find((item) => item.name === cantonName);

  if (previousactivecanton) {
    previousactivecanton.classList.remove('active');
  }

  cantonElement.classList.add('active');
  previousactivecanton = cantonElement;

  if (cantonData) {
    tooltip.innerHTML = `
      <b>${cantonData.name}</b><br>
      Erwerbstätige: <b>${cantonData.erwerbstätige}</b><br>
      Erwerblose: <b>${cantonData.erwerblose}</b>
    `;
  } else {
    tooltip.innerHTML = 'Informationen werden geladen...';
  }
}

function showTooltip(event) {
  const tooltip = document.getElementById('tooltip-map');
  const offsetX = 10;
  const offsetY = 10;
  tooltip.style.left = event.clientX + window.scrollX + offsetX + 'px';
  tooltip.style.top = event.clientY + window.scrollY + offsetY + 'px';
  tooltip.classList.remove('do-not-display');
}

function moveTooltip(event) {
  const tooltip = document.getElementById('tooltip-map');
  const offsetX = 10;
  const offsetY = 10;
  tooltip.style.left = event.clientX + window.scrollX + offsetX + 'px';
  tooltip.style.top = event.clientY + window.scrollY + offsetY + 'px';
}

function hideTooltip(event) {
  const tooltip = document.getElementById('tooltip-map');
  tooltip.classList.add('do-not-display');
  if (previousactivecanton) {
    previousactivecanton.classList.remove('active');
  }
}

function aggregateAgeGroupData(data) {
  let aggregatedData = {
    '15_24': 0,
    '25_34': 0,
    '35_44': 0,
    '45_54': 0,
    '55_64': 0,
    '65_plus': 0,
  };

  data.forEach((item) => {
    aggregatedData['15_24'] += item.alterskategorie['15_24'] || 0;
    aggregatedData['25_34'] += item.alterskategorie['25_34'] || 0;
    aggregatedData['35_44'] += item.alterskategorie['35_44'] || 0;
    aggregatedData['45_54'] += item.alterskategorie['45_54'] || 0;
    aggregatedData['55_64'] += item.alterskategorie['55_64'] || 0;
    aggregatedData['65_plus'] += item.alterskategorie['65_plus'] || 0;
  });

  return aggregatedData;
}

function aggregateEducationData(data) {
  let aggregatedData = {
    keineAusbildungAbgeschlossen: 0,
    sekundarstufeI: 0,
    sekundarstufeII: 0,
    tertiärstufe: 0,
  };

  data.forEach((item) => {
    aggregatedData.keineAusbildungAbgeschlossen +=
      item.ausbildungsstand.keineAusbildungAbgeschlossen || 0;
    aggregatedData.sekundarstufeI += item.ausbildungsstand.sekundarstufeI || 0;
    aggregatedData.sekundarstufeII +=
      item.ausbildungsstand.sekundarstufeII || 0;
    aggregatedData.tertiärstufe += item.ausbildungsstand.tertiärstufe || 0;
  });

  return aggregatedData;
}

// Function to aggregate employment
function aggregateEmploymentLevelData(data) {
  let aggregatedData = {
    nichtZuteilbare: 0,
    ungelernt: 0,
    qualifiziertManuell: 0,
    qualifiziertNichtManuell: 0,
    intermediaere: 0,
    akademische: 0,
    andereSelbst: 0,
    freieBerufe: 0,
    oberstesManagement: 0,
  };

  data.forEach((item) => {
    aggregatedData.nichtZuteilbare += item.arbeitsniveau.nichtZuteilbare;
    aggregatedData.ungelernt += item.arbeitsniveau.ungelernt;
    aggregatedData.qualifiziertManuell +=
      item.arbeitsniveau.qualifiziertManuell;
    aggregatedData.qualifiziertNichtManuell +=
      item.arbeitsniveau.qualifiziertNichtManuell;
    aggregatedData.intermediaere += item.arbeitsniveau.intermediaere;
    aggregatedData.akademische += item.arbeitsniveau.akademische;
    aggregatedData.andereSelbst += item.arbeitsniveau.andereSelbst;
    aggregatedData.freieBerufe += item.arbeitsniveau.freieBerufe;
    aggregatedData.oberstesManagement += item.arbeitsniveau.oberstesManagement;
  });

  return aggregatedData;
}

document.addEventListener('DOMContentLoaded', async function () {
  await initializeCharts();
});
