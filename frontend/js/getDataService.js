async function getData(link) {
  try {
    const response = await axios.get(link);
    const data = response.data;
    return data;
  } catch (error) {
    console.error('There has been a problem with your fetch operation:', error);
    return null;
  }
}

export { getData };
